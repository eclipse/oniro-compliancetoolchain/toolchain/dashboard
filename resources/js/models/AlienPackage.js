// TODO
import SourceFile from "./SourceFile";
import BinaryPackage from "./BinaryPackage";

import _ from "lodash";

export default class AlienPackage {
	id = null;

	anomalies = [];

	name = null;
	version = null;
	revision = null;
	variant = null;

	tags = {
		project: [],
		release: [],
		distro: [],
		machine: [],
		image: []
	};

	debian_matching = {
		name: "",
		version: "",
		ip_matching_files: 0
	};

	statistics = {
		files: {
			audit_total: 0,
			audit_done: 0,
			audit_to_do: 0,
			upstream_source_total: 0,
			unknown_provenance: 0,
			known_provenance: 0,
			total: 0
		},
		licenses: {
			license_scanner_findings: [],
			license_audit_findings: {
				main_licenses: [],
				all_licenses: []
			}
		},
		aggregate: false
	};

	source_files = [];
	binary_packages = [];

	cveStatus = {}

	cveScoreSum = 0;

	metadata = false;
	cve_metadata = false;
	is_main_variant = false;

	constructor(data = {}) {
		if (data.id) this.id = data.id;
		if (data.tags) this.tags = data.tags;
		if (data.name) this.name = data.name;
		if (data.version) this.version = data.version;
		if (data.revision) this.revision = data.revision;
		if (data.variant) this.variant = data.variant;
		if (data.debian_matching) this.debian_matching = data.debian_matching;
		if (data.statistics) this.statistics = data.statistics;
		if (data.source_files) this.source_files = data.source_files;
		if (data.binary_packages) this.binary_packages = data.binary_packages;
		if (data.session_state) this.session_state = data.session_state
		if (data.metadata) this.metadata = data.metadata
		if (data.cveStatus) this.cveStatus = data.cveStatus
		if (data.cveScoreSum) this.cveScoreSum = data.cveScoreSum
		if (data.anomalies) this.anomalies = data.anomalies

		this.anomalies = this.anomalies.map(item => {
			return { 
				...item,           
				package_id: data.id    
			};
		});

		if(this.anomalies.length > 0) this.hasAnomalies = true;

		this.is_main_variant = data.is_main_variant
		this.cve_metadata = data.cve_metadata && data.cve_metadata.issue != null ? data.cve_metadata : { issue: [] }

		this.layer = data.layer ? data.layer : {}

		// sort stati
		this.cve_metadata.issue.sort(function (a, b) {
			var aw = {
				"Unpatched": 3,
				"Patched": 1,
				"Ignored": 2,
			}

			if (aw[a.status] > aw[b.status]) return -1;
			if (aw[a.status] < aw[b.status]) return 1;
			if (aw[a.status] == aw[b.status]) return 0;
		})

		// sort vals
		this.cve_metadata.issue.sort(function (a, b) {
			if (a.status == b.status) {
				let aScore = Math.max(a.scorev2, a.scorev3);
				let bScore = Math.max(b.scorev2, b.scorev3);

				if (aScore > bScore) return -1;
				if (bScore < aScore) return 1;
				if (bScore == aScore) return 0;
			}
		})
		this.collectCves();
		this.processStats();
	}

	processStats() {
		var filestats = this.statistics.files;
		this.progress =
			filestats.audit_total == 0
				? 100
				: parseInt(
					(filestats.audit_done / filestats.audit_total) * 100
				);
		this.workload = filestats.audit_done;
		this.workload_total = filestats.audit_total;
		this.match = this.debian_matching
			? (this.debian_matching.ip_matching_files /
				filestats.upstream_source_total) *
			100
			: 0;
	}

	collectCves() {
		if (this.cveStatus.patched > 0 || this.cveStatus.unpatched > 0 || this.cveStatus.ignored > 0) this.isCve = true
		if (this.cveStatus['unpatched'] > 0) this.isUnpatched = true;
		if (this.session_state.uploaded) this.flags = "isNew"
	}

	setVariantTags() {
		if (!this.isVariant) return;

		let variant_machines = {};

		// 	"it is assumed that there is only 1 release and 1 image in paths. thus these two infos are overwritten."
		for (var a = 0; a < this.meta_source_files.length; a++) {
			if (this.meta_source_files[a].paths.length > 0) {
				this.meta_source_files[a].machines = [];
				for (
					var i = 0;
					i < this.meta_source_files[a].paths.length;
					i++
				) {
					const tags = this.meta_source_files[a].paths[i].split("/");
					if (tags.length >= 4) {
						this.meta_source_files[a].release = tags[2];
						this.meta_source_files[a].image = tags[3];
						// example: libgloss missing machines part
						if (tags.length > 4) {
							variant_machines[tags[4]] = tags[4];
							this.meta_source_files[a].machines.push(tags[4]);
						} else {
							this.meta_source_files[a].machines.push("all");
						}
					} else {
						console.error(
							"paths length breaking conventions, setVariantTags not possible"
						);
					}
				}
			} else {
				this.meta_source_files[a].machines = ["all"];
			}

			if (Object.values(variant_machines).length > 0) {
				this.variant_machines = Object.values(variant_machines);
			}
		}
	}
}
