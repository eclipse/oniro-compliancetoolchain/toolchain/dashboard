ARG APP_ENV=production
ARG NODE_VERSION=22.11.0

FROM node:${NODE_VERSION}-alpine AS node

FROM webdevops/php-nginx:8.2-alpine@sha256:ce439471c5b1836f0730a7b43401d733634c6693562a3a190af49429d726b3bc as base

COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

# restore intermediate mess
USER root

RUN apk add oniguruma-dev postgresql-dev libxml2-dev 
RUN apk add postgresql-client

# install build systems
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN apk add --update npm

ENV WEB_DOCUMENT_ROOT /app/public
ENV APP_ENV $APP_ENV

WORKDIR /app

# copy with chown to prevent slow chown bug (also known as 'now i finally survive until deployment ends' fix)
COPY --chown=application:application . .
COPY ./infrastructure/nginx/a4f.conf /opt/docker/etc/nginx/conf.d

# do not exec composer and npm as root
USER application

# install backend & frontend dependencies & compile
RUN composer install --no-interaction --optimize-autoloader --no-dev
RUN npm install
RUN npm run prod

RUN mv -f .env.example .env

# reload new .env
RUN php artisan config:clear

# sessions will be invalidated @ every deployment. This should work as long we don't need to use encrypt() inside our app
RUN php artisan key:generate --force

# reload new .env
RUN php artisan config:clear

# optimizations
RUN php artisan config:cache
RUN php artisan route:cache
RUN php artisan view:cache

# restore bash user
USER root

# reload new .env
RUN php artisan config:clear

EXPOSE 5000

FROM base as jenkinsci
ARG JENKINS_GROUP_ID=2000
ARG JENKINS_USER_ID=2000
RUN groupadd -g $JENKINS_GROUP_ID jenkins && \
    useradd -ms /bin/bash --no-user-group -g $JENKINS_GROUP_ID -u $JENKINS_USER_ID jenkins

FROM base as build
# Use the .dockerignore file to exclude files from this COPY command
COPY . /var/www/html/
